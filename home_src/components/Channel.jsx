import React from "react";
import Field from "./Field.jsx";

export default class Channel extends React.Component {
    constructor() {
        super();
    }

    getCheckedFields()
    {
        var fields = [];
        this.props.channel.fields.map((field) => (
            fields = fields.concat(this.refs[field.name].getCheckedFields())
        ));
        return fields;
    }

    render() {
        return (
            <tr><td><table><tbody>
            <tr>
                <td  colSpan="4">
                    <span class="channelHeader">{this.props.channel.name}</span>
                    <span class="channelStatus">
                        {this.props.channel.status ? " (" + this.props.channel.status + ")" : ""}
                    </span>
                </td>
            </tr>
            {
                this.props.channel.fields.map((field,index) => (
                            <Field key={index}
                                   ref={field.name}
                                   field={field}
                                   layout={this.props.layout} />
                        ))
            }
            </tbody></table></td></tr>
        );
    }
}

