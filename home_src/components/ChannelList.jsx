import React from "react";
import Channel from "./Channel.jsx";

export default class ChannelList extends React.Component {
    constructor() {
        super();
    }

    getCheckedFields()
    {
        var fields = [];
        this.props.channels.map((channel) => (
            fields = fields.concat(this.refs[channel.name].getCheckedFields())
        ));
        return fields;
    }

    render() {
        return (
            <table><tbody>
            {
                this.props.channels.map((item) => (
                    <Channel ref={item.name} key={item.name} channel={item} layout={this.props.layout}/>
                ))
            }
            </tbody></table>
        );
    }
}

