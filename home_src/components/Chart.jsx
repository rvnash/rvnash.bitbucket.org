import React from "react";
import Dygraph from "dygraphs";
import styles from 'dygraphs';
import $ from "jquery";

export default class Chart extends React.Component {

    constructor()
    {
        super();

        this.state = {
            g: null,
            data: null,
            channels: []
        };
    }

    // Return graph data
    // Need to implement coallescing data from different channels into one data stream for Dygraph
    getGraphData()
    {
        return this.state.data;
    }
    
    highlightCallback(event, x, points, row, seriesName)
    {
        this.myData.props.layout.highlightChannels(this,x,points);
    }
    
    unhighlightCallback(event)
    {
        this.myData.props.layout.highlightChannels(this,null,null);
    }

    loadGraph()
    {
        var labels = ['Time'];
        this.state.fields.map((mfield,i) => {
            var name = "" + mfield.channel + "_" + mfield.index; // Give the channel_index as the name to find it later
            labels.push(name);

        });
        var dygraphOptions =    {
                                    file: () => this.getGraphData(),
                                    axes: {
                                        y: {
                                          axisLabelWidth: 20
                                        }
                                    },
                                    connectSeparatedPoints: true,
                                    highlightCallback: this.highlightCallback,
                                    unhighlightCallback: this.unhighlightCallback,
                                    showLabelsOnHighlight: false,
                                    labels: labels,
                                 };
        if (this.state.g != null) {
            this.state.g.updateOptions(dygraphOptions );
            this.props.layout.highlightChannels(this.state.g,null,null);
        } else {
            this.state.g = new Dygraph(document.getElementById("graphdiv"),
                                        () => this.getGraphData(),
                                        dygraphOptions);
            this.state.g.myData = this;
            this.props.layout.highlightChannels(this.state.g,null,null);
        }
        this.refs.spinner.style.visibility = "hidden";

    }
    
    // When a URL load of the field's stream is loaded, this is called.
    // data - the raw datastream from the URL call
    // iGraph - Which graph element this stream represents
    // maxChannels - How many things we are graphing
    channelLoaded(data, iGraph, maxChannels, field)
    {
        var feed = data.feeds;
        var fieldPicker = "field"+field.index
        var iCurrentData = 0;
        for (var i = 0; i < feed.length; i++) {
            var dt = new Date(feed[i].created_at);
            var val = parseFloat(feed[i][fieldPicker]);
            if (isNaN(val)) continue;
            // Find the point to insert or update
            var updated = false;
            while (iCurrentData < this.state.data.length) {
                if (dt.getTime() == this.state.data[iCurrentData][0].getTime()) {
                    // Update the current point with this value.
                    this.state.data[iCurrentData][iGraph+1] = val;
                    updated = true;
                    break;
                }
                if (dt.getTime() < this.state.data[iCurrentData][0].getTime()) break;
                iCurrentData++;
            }
            if (!updated) {
                var point = [dt];
                for (var j = 0; j < maxChannels; j++) point.push(null);
                this.state.data.splice(iCurrentData,0,point);
                this.state.data[iCurrentData][iGraph+1] = val;
            }
            iCurrentData++;
        }
        this.state.fieldsToLoad--;
        if (this.state.fieldsToLoad == 0) {
            this.loadGraph();
        }
    }

    // 
    // fields - array of fields to display
    // daysToGraph - how many days to get
    chartFields(fields, daysToGraph)
    {
        this.refs.spinner.style.visibility = "visible";
        this.state.fieldsToLoad = fields.length;
        this.state.fields = fields;
        this.state.data = [];
        if (this.state.fields.length == 0) {
            this.loadGraph();
            this.refs.spinner.style.visibility = "hidden";
        } else {
            for (var i = 0; i < fields.length; i++) {
                var field = fields[i];
                var url = "https://api.thingspeak.com/channels/" + field.channel + "/fields/" + field.index + ".json?days=" + daysToGraph
                $.getJSON( url, (function(_this, i, maxChannels, field) {
                                    return function(data) {
                                        _this.channelLoaded(data,i,maxChannels,field);
                                    }
                                 })(this,i,fields.length,field) );
            }
        }
    }

    render()
    {
        return <div class="chartContainer">
                    <div id="graphdiv" class="graphBox" />
                    <div class="graphBoxWSpinner" >
                        <img src="spin.gif" class="spinCenter" ref="spinner"/>
                    </div>
               </div>
    };
}

