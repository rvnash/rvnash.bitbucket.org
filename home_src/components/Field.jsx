import React from "react";
import $ from "jquery";

function formatNum(num, length) {
    var r = "" + num;
    while (r.length < length) {
        r = "0" + r;
    }
    return r;
};


export default class Field extends React.Component {
    constructor() {
        super();
        this.fIsLoading = false;
        this.url = null;
        this.state = {
            color: "rgb(0,0,0)",
            tmpValue: null,
            tmpTime: null,
            lastValue: '--',
            lastValueTime: '--',
        };
    }

    loadLastValue()
    {
        if (this.fIsLoading) return;
        this.fIsLoading = true;
        $.getJSON( this.url,
                      (function(_this) {
                        _this.fIsLoading = false;
                        return function(data) {
                            var lastValueTime = data.created_at;
                            var date = new Date(lastValueTime);
                            var lastValue = parseFloat(data['field' + _this.props.field.index]);
                            if (isNaN(lastValue)) {
                                lastValue = '--';
                            } else {
                                lastValue = lastValue.toFixed(_this.props.field.digitsAfterDecimal);
                            }
                            if (lastValueTime != null) {
                                _this.setState(
                                    {
                                        lastValue: lastValue,
                                        lastValueTime: date.getFullYear() + "-" + formatNum(date.getMonth() + 1,2) + "-" + formatNum(date.getDate(),2)
                                                       + " " + formatNum(date.getHours(),2) + ":" + formatNum(date.getMinutes(),2) + ":" + formatNum(date.getSeconds(),2)
                                    }
                                );
                            } else {
                                _this.setState(
                                    {
                                        lastValue: '--',
                                        lastValueTime: '--'
                                    }
                                );
                            }
                          }
                    })(this)
                );
    }

    componentWillMount()
    {
        this.props.field.uiField = this; // Set up the field object so that it knows its UI element
        this.url = 'https://api.thingspeak.com/channels/' + this.props.field.channel + '/fields/' + this.props.field.index + "/last.json";
        this.loadLastValue();
        // setInterval(this.loadLastValue.bind(this), 30000);
    }

    getCheckedFields()
    {
        if (this.refs.checkbox.checked) {
            return [this.props.field];
        } else {
            return [];
        }
    }

    checkChanged()
    {
        this.props.layout.onGraph(); // Draw the graph on every click or unclick
    }

    render() {
        var nameStyle;
        var valueStyle;
        var time;
        var value;
        
        if (this.state.tmpTime == null) {
            time = this.state.lastValueTime;
            value = this.state.lastValue + this.props.field.units;
            nameStyle = {
              color: this.state.color,
              fontWeight: "normal"
            };
            valueStyle = {
              color: 'rgb(0,0,0)',
              fontWeight: "normal"
            };
        } else {
            var date = new Date(this.state.tmpTime);
            time = date.getFullYear() + "-" + formatNum(date.getMonth() + 1,2) + "-" + formatNum(date.getDate(),2)
                                                       + " " + formatNum(date.getHours(),2) + ":" + formatNum(date.getMinutes(),2) + ":" + formatNum(date.getSeconds(),2)
            if (this.state.tmpValue == null) {
                this.state.tmpValue = '--';
            } else {
                this.state.tmpValue = this.state.tmpValue.toFixed(this.props.field.digitsAfterDecimal);
            }
            value = this.state.tmpValue + this.props.field.units;
            nameStyle = {
              color: this.state.color,
              fontWeight: "normal"
            };
            valueStyle = {
              color: this.state.color,
              fontWeight: "normal"
            };
        }
        return (
            <tr key={this.props.field.index}>
                <td>
                    <input ref="checkbox" type="checkbox" onChange={(e) => this.checkChanged(e)}/>
                </td>
                <td>
                    <div style={nameStyle}>{this.props.field.uiname}</div>
                </td>
                <td>
                    <div style={valueStyle}>{value}</div>
                </td>
                <td>
                    <div style={valueStyle}>{time}</div>
                </td>
            </tr>
        );
    }
}

