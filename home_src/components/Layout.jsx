import React from "react";
import $ from "jquery";
import Chart from "./Chart.jsx"
import ChannelList from "./ChannelList.jsx"
import TimeSelector from "./TimeSelector.jsx"
import Footer from "./Footer.jsx"
import Header from "./Header.jsx"

export default class Layout extends React.Component {
    constructor() {
        super();
        this.state = {
            fieldsGraphed: [],
            channelsLoaded: false,
            channels:   [
            /* Format Example:
                          {
                            channel:157175,
                            name: null,
                            description: null,
                            status: "192.168.1.25",
                            fields: [
                                {
                                   channel:157175, // Redundant reference
                                   index:1,
                                   name:"temperature,�F,1"
                                   uiname: "temperature",
                                   units: "�F",
                                   digitsAfterDecimal: 1,
                                }
                            ]
                          },
                          {
                            channel:159291,
                            name: null,
                            description: null,
                            fields: []
                          }
                          */
                        ]
        };
        this.loadConfig();
    };

    loadConfig()
    {
        var url = "config.json";
        $.getJSON( url,
                      (function(_this) { // Capture this
                        return function(data) {
                          for (var i = 0; i < data.channels.length;i++) {
                            _this.state.channels.push({
                                              channel:data.channels[i],
                                              name: null,
                                              description: null,
                                              status: null,
                                              fields: []
                                             });
                          }
                          _this.loadMetaData();
                        }
                      }
                      )  (this)
                );
    }

    parseName(name)
    {
      var str = name.split(",");
      if (str.length != 3) {
        return {
          uiname: name,
          units: "",
          digitsAfterDecimal: 2
        };
      } else {
        return {
          uiname: str[0],
          units: str[1],
          digitsAfterDecimal: str[2]
        };
      }
    }

    loadMetaData()
    {
      var loaded = 0;
      var channels = [];
      for (var i = 0; i < this.state.channels.length; i++)
      {
        channels.push({channel:this.state.channels[i].channel, name:null, description:null, fields:[]});
      }
      for (var i = 0; i < channels.length; i++)
      {
        var url = 'https://api.thingspeak.com/channels/' + channels[i].channel + '/feeds.json?results=1&status=true';
        ++loaded;
        $.getJSON( url,
                      (function(i,channels,_this) { // Capture the local variable i
                        return function(data) {
                            channels[i].name = data.channel.name;
                            channels[i].description = data.channel.description;
                            channels[i].status = "";
                            if (data.feeds.length >= 1) {
                                channels[i].status = data.feeds[0].status;
                            }
                            for (var j = 1; j <= 8; j++) {
                              var name = data.channel["field"+j];
                              if (name != null && name != "") {
                                var parse = _this.parseName(name);
                                channels[i].fields.push(
                                  {
                                   channel:channels[i].channel,
                                   index:j,
                                   name:name,
                                   uiname: parse.uiname,
                                   units: parse.units,
                                   digitsAfterDecimal: parse.digitsAfterDecimal
                                 });
                              }
                            }
                            --loaded;
                            if (loaded == 0) {
                              _this.loadingMetadataComplete(channels);
                            }
                          }
                    })(i,channels,this)
        );
      }
    };

    loadingMetadataComplete(channels)
    {
        this.setState({
                        channels:channels,
                        channelsLoaded: true
                      }
                     );
    };

    onGraph()
    {
        this.state.fieldsGraphed.map( (field) => {
            field.uiField.setState( {
                                    color: "rgb(0,0,0)",
                                    tmpValue: null,
                                    tmpTime: null
                                    }
                                   );
        });

        var daysToGraph = 1;
        switch (this.refs.timeSelector.state.timeSel) {
          case "day":
            daysToGraph = 1;
          break;
          case "week":
            daysToGraph = 7;
          break;
          case "month":
            daysToGraph = 31;
          break;
          case "year":
            daysToGraph = 365;
          break;
          case "decade":
            daysToGraph = 365*10;
          break;
        }
        this.state.fieldsGraphed = this.refs.channelList.getCheckedFields();
        this.refs.chart.chartFields(this.state.fieldsGraphed, daysToGraph);
    }
    
    highlightChannels(dygraph, x, points)
    {
        if (points == null) {
          this.state.fieldsGraphed.map( (field,i) => {
              field.uiField.setState( {
                                      color: dygraph.colors_[i],
                                      tmpValue: null,
                                      tmpTime: null
                                      }
                                    );
          } );
        } else {
          this.state.fieldsGraphed.map( (field,i) => {
              var i;
              for (i = 0; i < points.length; i++) {
                var point = points[i];
                if ("" + field.channel + "_" + field.index == point.name) {
                  field.uiField.setState( {
                                          color: dygraph.colors_[i],
                                          tmpValue: points[i].yval,
                                          tmpTime: points[i].xval
                                          }
                                        );
                  break;
                }
              }
              if (i == points.length) {
                field.uiField.setState( {
                        color: dygraph.colors_[i],
                        tmpValue: null,
                        tmpTime: x
                        }
                      );
              }
          } );
        }
    }

    render()
    {
        return (
            <div class="viewportFixed">
            {
                (this.state.channelsLoaded) ? 
                    <div class="pageDiv">
                        <Header />
                        <Chart ref="chart" layout={this}/>
                        <TimeSelector ref="timeSelector" layout={this} />
                        <div class="chartBox">
                          <ChannelList ref="channelList" channels={this.state.channels} layout={this}/>
                        </div>
                        
                        <Footer />
                    </div>
                :
                    <div />
            }
            </div>
        );
    };
}

