import React from "react";

export default class TimeSelector extends React.Component {
    constructor() {
        super();
        this.state = {
            timeSel:"day"
        }
    }

    changedTime(e)
    {
        this.state.timeSel = e.currentTarget.value;
        this.setState({
            timeSel: e.currentTarget.value
        });
        this.props.layout.onGraph();
    }

    render() {
        return (
            <div>
                <form>
                <input onChange={(e) => this.changedTime(e)} type="radio" name="duration" value="day" checked={this.state.timeSel == "day"} />Day
                <input onChange={(e) => this.changedTime(e)} type="radio" name="duration" value="week" checked={this.state.timeSel == "week"}/>Week
                <input onChange={(e) => this.changedTime(e)} type="radio" name="duration" value="month" checked={this.state.timeSel == "month"}/>Month
                <input onChange={(e) => this.changedTime(e)} type="radio" name="duration" value="year" checked={this.state.timeSel == "year"}/>Year
                <input onChange={(e) => this.changedTime(e)} type="radio" name="duration" value="decade" checked={this.state.timeSel == "decade"}/>Decade
                <br/>
                </form>
            </div>
        );
    }
}

